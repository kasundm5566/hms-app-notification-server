package hms.appnotification.api.model

data class RegisterDeviceRequest(
        val deviceId: String,
        val userId: String,
        val platform: String,
        val platformVersion: String,
        val notificationToken: String
)

data class RegisterDeviceResponse(
        val deviceId: String,
        val userId: String
)