package hms.appnotification.impl

import hms.app.notification.server.model.notification.NotifyTopicRequest
import hms.app.notification.server.model.notification.NotifyTopicResponse
import hms.app.notification.server.model.notification.NotifyUsersRequest
import hms.app.notification.server.model.notification.NotifyUsersResponse
import hms.appnotification.AppNotificationService
import hms.appnotification.api.model.RegisterDeviceRequest
import hms.appnotification.api.model.RegisterDeviceResponse
import hms.appnotification.client.FirebaseMessagingClient
import hms.appnotification.integrations.fcm.DispatchedMessagesForUserResponse
import hms.appnotification.integrations.fcm.UpdatedFirebaseMessagingClient
import hms.appnotification.integrations.fcm.UpdatedFirebaseNotificationBody
import hms.appnotification.repo.entity.DeviceRegistrationEntity
import hms.appnotification.repo.entity.DeviceRegistrationEntityId
import hms.appnotification.repo.repository.DeviceRegistrationRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.LocalDateTime

@Service
@ConfigurationProperties(prefix = "firebase.cloud.messaging.config")
@EnableConfigurationProperties
class AppNotificationServiceImpl(val deviceRegistrationRepository: DeviceRegistrationRepository,
                                 val firebaseMessagingClient: FirebaseMessagingClient,
                                 val updatedFirebaseMessagingClient: UpdatedFirebaseMessagingClient) : AppNotificationService {

    var serverKey: List<String>? = null
    var defaultFcmTopicName: String? = null
    var defaultDispatchType: String? = null

    companion object {
        val LOGGER: Logger = LoggerFactory.getLogger(AppNotificationServiceImpl::class.java)
    }

    override fun registerDevice(deviceRegistrationRequest: Mono<RegisterDeviceRequest>): Mono<RegisterDeviceResponse> {
        return deviceRegistrationRequest.doOnSuccess { request ->
            LOGGER.info("Going to register device [$request]")
        }.map { request ->
            DeviceRegistrationEntity(DeviceRegistrationEntityId(request.deviceId, request.userId), request.platform,
                    request.platformVersion, request.notificationToken, "", "")
        }.flatMap { deviceRegEntity ->
            //TODO make repo calls Mono
            val registrationEntity = deviceRegistrationRepository.findById(deviceRegEntity.entityId)
            if (registrationEntity.isPresent) {
                deviceRegEntity.createdDate = registrationEntity.get().createdDate
                deviceRegEntity.lastUpdatedDate = LocalDateTime.now().toString()
            } else {
                deviceRegEntity.createdDate = LocalDateTime.now().toString()
                deviceRegEntity.lastUpdatedDate = LocalDateTime.now().toString()
            }
            deviceRegistrationRepository.save(deviceRegEntity)
            firebaseMessagingClient.subscribeUserToTopic(defaultFcmTopicName
                    ?: "", deviceRegEntity.firebaseIdToken, serverKey?.getOrNull(0)
                    ?: "", deviceRegEntity.entityId.userId)
                    .map { resp ->
                        RegisterDeviceResponse(deviceRegEntity.entityId.deviceId, deviceRegEntity.entityId.userId)
                    }
        }.doOnSuccess { success ->
            LOGGER.info("Device registered for [$success]")
        }
    }

    override fun notifyUsers(notificationRequest: Mono<NotifyUsersRequest>): Mono<NotifyUsersResponse> {
        return notificationRequest.flatMap { request ->
            LOGGER.info("Notifying users for request [$request]")
            var dispatchType = defaultDispatchType.toString()
            if (request.dispatchType != null) {
                dispatchType = request.dispatchType.toString()
            }
            when (dispatchType) {
                "new" -> {
                    var msisdn = ""
                    Flux.fromIterable(request.userIds)
                            .map { userId ->
                                msisdn = userId
                                getFcmTokens(userId)
                            }
                            .collectList()
                            .flatMap { firebaseTokens ->
                                updatedFirebaseMessagingClient.notifyUsersAndroid(request.requestId, firebaseTokens.flatten(), UpdatedFirebaseNotificationBody(request.messageContent.title, request.messageContent.body), request.additionalParameters, request.clickAction, msisdn)
                            }
                            .doOnSuccess { firebaseNotifyResp -> LOGGER.info("Notification response received [$firebaseNotifyResp]") }
                            .doOnError { error -> LOGGER.error("Unexpected error occurred while notifying the users [$error]") }
                            .map { firebaseNotifyResp ->
                                //TODO error mappings
                                NotifyUsersResponse(request.requestId, "S1000")
                            }
                }
                "legacy" -> {
                    Flux.fromIterable(request.userIds)
                            .map { userId -> getFcmTokens(userId) }
                            .collectList()
                            .flatMap { firebaseTokens ->
                                firebaseMessagingClient.notifyUsersAndroid(firebaseTokens.flatten(), request.messageContent, serverKey!![0])
                            }
                            .doOnSuccess { firebaseNotifyResp -> LOGGER.info("Notification response received [$firebaseNotifyResp]") }
                            .doOnError { error -> LOGGER.error("Unexpected error occurred while notifying the users [$error]") }
                            .map { firebaseNotifyResp ->
                                //TODO error mappings
                                NotifyUsersResponse(request.requestId, "S1000")
                            }
                }
                else -> {
                    Flux.fromIterable(request.userIds)
                            .map { userId -> getFcmTokens(userId) }
                            .collectList()
                            .flatMap { firebaseTokens ->
                                firebaseMessagingClient.notifyUsersAndroid(firebaseTokens.flatten(), request.messageContent, serverKey!![0])
                            }
                            .doOnSuccess { firebaseNotifyResp -> LOGGER.info("Notification response received [$firebaseNotifyResp]") }
                            .doOnError { error -> LOGGER.error("Unexpected error occurred while notifying the users [$error]") }
                            .map { firebaseNotifyResp ->
                                //TODO error mappings
                                NotifyUsersResponse(request.requestId, "S1000")
                            }
                }
            }
        }
    }

    private fun getFcmTokens(userId: String): List<String> {
        //TODO We need to support multiple FCM keys per userId
        return deviceRegistrationRepository.findByUserId(userId)
    }

    override fun notifyTopic(notificationRequest: Mono<NotifyTopicRequest>): Mono<NotifyTopicResponse> {
        return notificationRequest.flatMap { request ->
            LOGGER.info("Notifying the topic subscribers for request [$request]")
            var dispatchType = defaultDispatchType.toString()
            if (request.dispatchType != null) {
                dispatchType = request.dispatchType.toString()
            }
            when (dispatchType) {
                "new" -> {
                    updatedFirebaseMessagingClient.notifyTopic(request.requestId, request.topic, UpdatedFirebaseNotificationBody(request.messageContent.title, request.messageContent.body), request.clickAction)
                            .doOnSuccess { firebaseNotifyResp -> LOGGER.info("Notification response received [$firebaseNotifyResp]") }
                            .doOnError { error -> LOGGER.info("Unexpected error occurred while notifying the topic [$error]") }
                            .map { firebaseNotifyResp ->
                                //TODO error code mappings
                                NotifyTopicResponse(request.requestId, "S1000")
                            }
                }
                "legacy" -> {
                    firebaseMessagingClient.notifyTopic(request.topic, request.messageContent, serverKey!![0])
                            .doOnSuccess { firebaseNotifyResp -> LOGGER.info("Notification response received [$firebaseNotifyResp]") }
                            .doOnError { error -> LOGGER.info("Unexpected error occurred while notifying the topic [$error]") }
                            .map { firebaseNotifyResp ->
                                //TODO error code mappings
                                NotifyTopicResponse(request.requestId, "S1000")
                            }
                }
                else -> {
                    firebaseMessagingClient.notifyTopic(request.topic, request.messageContent, serverKey!![0])
                            .doOnSuccess { firebaseNotifyResp -> LOGGER.info("Notification response received [$firebaseNotifyResp]") }
                            .doOnError { error -> LOGGER.info("Unexpected error occurred while notifying the topic [$error]") }
                            .map { firebaseNotifyResp ->
                                //TODO error code mappings
                                NotifyTopicResponse(request.requestId, "S1000")
                            }
                }
            }
        }
    }

    override fun retrieveMessages(userId: String, firebaseToken: String, start: Int, limit: Int): Mono<DispatchedMessagesForUserResponse> {
        return updatedFirebaseMessagingClient.retrieveMessagesSentForUser(userId, firebaseToken, start, limit)
    }
}