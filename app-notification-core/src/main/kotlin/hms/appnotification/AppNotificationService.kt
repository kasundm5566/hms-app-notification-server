/*
 * (C) Copyright 2010- 2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appnotification

import hms.app.notification.server.model.notification.NotifyTopicRequest
import hms.app.notification.server.model.notification.NotifyTopicResponse
import hms.app.notification.server.model.notification.NotifyUsersRequest
import hms.app.notification.server.model.notification.NotifyUsersResponse
import hms.appnotification.api.model.RegisterDeviceRequest
import hms.appnotification.api.model.RegisterDeviceResponse
import hms.appnotification.integrations.fcm.DispatchedMessagesForUserResponse
import reactor.core.publisher.Mono

interface AppNotificationService {

    fun registerDevice(deviceRegistrationRequest: Mono<RegisterDeviceRequest>): Mono<RegisterDeviceResponse>

    fun notifyUsers(notificationRequest: Mono<NotifyUsersRequest>): Mono<NotifyUsersResponse>

    fun notifyTopic(notificationRequest: Mono<NotifyTopicRequest>): Mono<NotifyTopicResponse>

    fun retrieveMessages(userId: String, firebaseToken: String, start: Int, limit: Int): Mono<DispatchedMessagesForUserResponse>

}