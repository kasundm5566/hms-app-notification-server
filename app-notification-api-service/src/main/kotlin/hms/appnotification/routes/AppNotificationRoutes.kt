package hms.appnotification.routes

import hms.app.notification.server.model.notification.NotifyTopicRequest
import hms.app.notification.server.model.notification.NotifyUsersRequest
import hms.appnotification.AppNotificationService
import hms.appnotification.api.model.RegisterDeviceRequest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.body
import org.springframework.web.reactive.function.server.bodyToMono
import org.springframework.web.reactive.function.server.router

@Configuration
class AppNotificationRoutes(var appNotificationService: AppNotificationService) {

    @Bean
    fun wellnessCoreApiRouter() = router {
        (accept(MediaType.APPLICATION_JSON) and "/appnotification/v1").nest {
            "/notify".nest {
                POST("/", {
                    ServerResponse.ok().json().body(appNotificationService.notifyUsers(it.bodyToMono<NotifyUsersRequest>()))
                })
            }
            "/topic".nest {
                POST("/", {
                    ServerResponse.ok().json().body(appNotificationService.notifyTopic(it.bodyToMono<NotifyTopicRequest>()))
                })
            }
            "/device".nest {
                POST("/register", {
                    ServerResponse.ok().json().body(appNotificationService.registerDevice(it.bodyToMono<RegisterDeviceRequest>()))
                })
            }
            "/messages".nest {
                GET("/{msisdn}/{firebaseToken}/start/{start}/limit/{limit}", {
                    ServerResponse.ok().json().body(appNotificationService.retrieveMessages(it.pathVariable("msisdn"), it.pathVariable("firebaseToken"), it.pathVariable("start").toInt(), it.pathVariable("limit").toInt()))
                })
            }
        }
    }

    fun ServerResponse.BodyBuilder.json() = contentType(MediaType.APPLICATION_JSON_UTF8)
}