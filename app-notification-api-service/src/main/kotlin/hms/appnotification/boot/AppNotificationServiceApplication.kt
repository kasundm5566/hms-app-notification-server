package hms.appnotification.boot

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan(basePackages = arrayOf("hms.appnotification"))
class AppNotificationServiceApplication {
}

fun main(args: Array<String>) {
    SpringApplication.run(AppNotificationServiceApplication::class.java, *args)
}