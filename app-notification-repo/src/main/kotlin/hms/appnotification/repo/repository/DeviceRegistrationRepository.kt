package hms.appnotification.repo.repository

import hms.appnotification.repo.entity.DeviceRegistrationEntity
import hms.appnotification.repo.entity.DeviceRegistrationEntityId
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import javax.transaction.Transactional

@Repository
@Transactional
interface DeviceRegistrationRepository : JpaRepository<DeviceRegistrationEntity, DeviceRegistrationEntityId> {

    @Query("SELECT d.firebaseIdToken FROM DeviceRegistrationEntity d WHERE d.entityId.userId = ?1")
    fun findByUserId(userId: String): List<String>

    @Query("SELECT d.entityId.userId FROM DeviceRegistrationEntity d WHERE d.firebaseIdToken = ?1")
    fun findByToken(token: String): String

}
