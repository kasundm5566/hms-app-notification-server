package hms.appnotification.repo.repository

import hms.appnotification.repo.entity.MessageDispatchHistoryEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

/**
 * Created by kasun on 7/23/18.
 */
@Repository
@Transactional
interface MessageDispatchHistoryRepository : JpaRepository<MessageDispatchHistoryEntity, Long> {

    @Query("select * from message_dispatch_history m where (m.dispatched_for=?1 AND m.firebase_token_id=?2) OR m.dispatched_for IN (select topic from user_topics where user_id=?1) order by timestamp desc limit ?4 offset ?3", nativeQuery = true)
    fun findMessagesByUserId(userId: String, firebaseToken: String, start: Int, limit: Int): List<MessageDispatchHistoryEntity>
}
