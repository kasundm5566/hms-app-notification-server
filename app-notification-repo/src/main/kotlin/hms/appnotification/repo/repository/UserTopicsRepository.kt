package hms.appnotification.repo.repository

import hms.appnotification.repo.entity.UserTopicsEntity
import hms.appnotification.repo.entity.UserTopicsEntityId
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

/**
 * Created by kasun on 7/23/18.
 */
@Repository
@Transactional
interface UserTopicsRepository : JpaRepository<UserTopicsEntity, UserTopicsEntityId> {
}