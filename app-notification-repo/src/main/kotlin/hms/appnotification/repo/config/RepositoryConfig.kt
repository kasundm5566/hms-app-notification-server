/*
 * (C) Copyright 2010- 2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appnotification.repo.config

import hms.appnotification.repo.entity.DeviceRegistrationEntity
import hms.appnotification.repo.entity.UserTopicsEntity
import hms.appnotification.repo.repository.DeviceRegistrationRepository
import hms.appnotification.repo.repository.UserTopicsRepository
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EnableJpaRepositories(basePackageClasses = [(DeviceRegistrationRepository::class), (UserTopicsRepository::class)])
@EntityScan(basePackageClasses = [(DeviceRegistrationEntity::class), (UserTopicsEntity::class)])
@EnableTransactionManagement
class RepositoryConfig