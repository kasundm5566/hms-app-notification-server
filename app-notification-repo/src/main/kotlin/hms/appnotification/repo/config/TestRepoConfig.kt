package hms.appnotification.repo.config

import hms.appnotification.repo.entity.DeviceRegistrationEntity
import hms.appnotification.repo.repository.DeviceRegistrationRepository
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EnableJpaRepositories(basePackageClasses = [(DeviceRegistrationRepository::class)])
@EntityScan(basePackageClasses = [(DeviceRegistrationEntity::class)])
@ComponentScan(basePackages = ["hms.research.wellness"])
@EnableTransactionManagement
class TestRepoConfig {
}