package hms.appnotification.repo.entity

import java.io.Serializable
import javax.persistence.Embeddable
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

/**
 * Created by kasun on 7/23/18.
 */

@Embeddable
data class UserTopicsEntityId(val userId: String, val topic: String) : Serializable {
    private constructor() : this(
            userId = "",
            topic = ""
    )
}

@Entity
@Table(name = "user_topics")
data class UserTopicsEntity(
        @EmbeddedId
        val entityId: UserTopicsEntityId
) {
    private constructor() : this(
            entityId = UserTopicsEntityId("", "")
    )
}