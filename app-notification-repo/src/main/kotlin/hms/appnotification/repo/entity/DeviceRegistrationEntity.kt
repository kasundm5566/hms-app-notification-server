package hms.appnotification.repo.entity

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.Embeddable
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table


@Embeddable
data class DeviceRegistrationEntityId(val deviceId: String,
                                      val userId: String) : Serializable {
    private constructor() : this(
            deviceId = "",
            userId = "")

}

@Entity
@Table(name = "device_registration")
data class DeviceRegistrationEntity(

        @EmbeddedId
        val entityId: DeviceRegistrationEntityId,

        val platform: String,
        val platformVersion: String,
        val firebaseIdToken: String,
        var createdDate: String,
        var lastUpdatedDate: String

) {
    private constructor() : this(
            entityId = DeviceRegistrationEntityId(deviceId = "", userId = ""),
            platform = "",
            platformVersion = "",
            firebaseIdToken = "",
            createdDate = "",
            lastUpdatedDate = ""
    )
}