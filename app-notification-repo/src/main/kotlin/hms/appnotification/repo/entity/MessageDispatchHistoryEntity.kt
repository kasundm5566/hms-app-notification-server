package hms.appnotification.repo.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.hibernate.annotations.CreationTimestamp
import java.time.LocalDateTime
import javax.persistence.*
import kotlin.jvm.Transient

/**
 * Created by kasun on 7/23/18.
 */
@Entity
@Table(name = "message_dispatch_history")
data class MessageDispatchHistoryEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @JsonIgnore
        val id: Long,

        @JsonProperty("dispatchedTime")
        val timestamp: String,
        val requestId: String,
        @JsonProperty("messageType")
        val dispatchedFor: String,
        val messageContent: String,
        @JsonIgnore
        val dispatcherType: String,
        @JsonIgnore
        val firebaseTokenId: String,
        @JsonProperty("activity")
        val activity: String,
        @JsonProperty("data")
        val data: String
) {
    private constructor() : this(
            id = 1,
            timestamp = "",
            requestId = "",
            dispatchedFor = "",
            messageContent = "",
            dispatcherType = "",
            firebaseTokenId = "",
            activity = "",
            data = ""
    )
}