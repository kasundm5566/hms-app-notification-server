package hms.research.wellness.utils

import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

fun getCurrentTime(): LocalDateTime = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS)

//2017-08-10 10:30:35
var dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
//2017-08-10
var dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

//04/01/2018 00:00:00
var dateFormatterDDMMYY: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")

//2017-mar
val dateFormatterWithYearMonth: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MMM")

//mar
val dateFormatterWithMonth: DateTimeFormatter = DateTimeFormatter.ofPattern("MMM")

/**
 * Create LocalDateTime from given string
 */
fun String.toLocalDateTime(): LocalDateTime = LocalDateTime.parse(this, dateTimeFormatter)

fun String.toLocalDate(): LocalDate = LocalDate.parse(this, dateFormatter)

/**
 * Convert LocalDateTime to string format
 */
fun LocalDateTime.toFormattedDateTimeString(): String = this.format(hms.research.wellness.utils.dateTimeFormatter)

/**
 * Convert LocalDateTime to string format which only contains date fragment
 */
fun LocalDateTime.toFormattedDateString(): String = this.format(hms.research.wellness.utils.dateFormatter)

/**
 * Convert LocalDate to string format which only contains date fragment
 */
fun LocalDate.toFormattedDateString(): String = this.format(hms.research.wellness.utils.dateFormatter)

/**
 * Create LocalDateTime from given Instant with UTC offset
 */
fun Instant.toLocalDateTime(): LocalDateTime = this.atOffset(ZoneOffset.UTC).toLocalDateTime()

/**
 * Get the date for Monday based on the day of week of the given date.
 */
fun LocalDateTime.dateOnMonday(): LocalDateTime = this.with(java.time.DayOfWeek.MONDAY)