/*
 * (C) Copyright 2010- 2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.research.wellness.utils.rest

import com.fasterxml.jackson.databind.ObjectMapper
import hms.app.notification.server.model.exception.ErrorResponse
import hms.app.notification.server.model.exception.UnauthorizedException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebExceptionHandler
import reactor.core.publisher.Mono


@Component
class SpringBootExceptionHandler : WebExceptionHandler {

    companion object {
        val LOGGER: Logger = LoggerFactory.getLogger(SpringBootExceptionHandler::class.java)
        val OBJECT_MAPPER = ObjectMapper()
    }

    override fun handle(exchange: ServerWebExchange?, ex: Throwable?): Mono<Void> {

        val responseObj: ErrorResponse = when (ex!!) {
            is NoSuchElementException -> {
                exchange!!.response.statusCode = HttpStatus.NOT_FOUND
                ErrorResponse(errorCode = "E0401",
                        errorDescription = ex.message ?: "Unauthorized")
            }

            is UnauthorizedException -> {
                exchange!!.response.statusCode = HttpStatus.UNAUTHORIZED
                ErrorResponse(errorCode = "E0401",
                        errorDescription = ex.message ?: "Unauthorized")
            }

            is Exception -> {
                LOGGER.error("Unexpected error", ex)
                exchange!!.response.statusCode = HttpStatus.INTERNAL_SERVER_ERROR
                ErrorResponse(errorCode = "E9999",
                        errorDescription = ex.message ?: "Unexpected error")
            }

            else -> {
                LOGGER.error("Unexpected error", ex)
                exchange!!.response.statusCode = HttpStatus.INTERNAL_SERVER_ERROR
                ErrorResponse(errorCode = "E9999",
                        errorDescription = ex.message ?: "Unexpected error")
            }
        }

        LOGGER.error("Created error response [$responseObj]")

        val dataBuffer: Mono<DataBuffer> = Mono.just(responseObj)
                .map { s -> exchange!!.response.bufferFactory().wrap(OBJECT_MAPPER.writeValueAsBytes(s)) }

        exchange!!.response.headers.contentType = MediaType.APPLICATION_JSON;
        return exchange.response.writeWith(dataBuffer)
    }
}