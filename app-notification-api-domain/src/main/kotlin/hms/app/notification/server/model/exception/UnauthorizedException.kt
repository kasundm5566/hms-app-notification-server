package hms.app.notification.server.model.exception

class UnauthorizedException(message: String?) : RuntimeException(message) {
}