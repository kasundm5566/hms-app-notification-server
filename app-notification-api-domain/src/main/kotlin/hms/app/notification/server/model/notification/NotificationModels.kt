package hms.app.notification.server.model.notification

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDateTime

data class NotifyAllRequest(
        val dispatcherId: String,
        val notificationContent: Map<String, Any>,
        val subscriberFetchCriteria: Map<String, Any>
)

data class NotifyTopicRequest(
        val requestId: String,
        val topic: String,
        val messageContent: NotificationContent,
        val dispatchType: String?,
        val clickAction: String
)

data class NotifyTopicResponse(
        val requestId: String,
        val status: String
)

data class NotifyUsersRequest(
        val requestId: String,
        val requestingSystemId: String,
        val userIds: List<String>,
        val messageContent: NotificationContent,
        val dispatchType: String?,
        val clickAction: String,
        val additionalParameters: Map<String, String>
)

data class NotifyAllResponse(
        val success: Int?,
        val error: Int?,
        val companyId: Long,
        val messageContent: NotificationContent
)

data class NotifyUsersResponse(
        val requestId: String,
        val status: String
)

data class NotificationContent(
        val title: String,
        val body: String,
        val targetScreen: String
)

data class UserNotification(
        val notificationId: Long,
        val notificationContent: NotificationContent,
        val category: String,
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") val time: LocalDateTime
)

data class NotificationInfoRequest(
        val companyId: Long
)

data class NotificationInfoResponse(
        val categories: List<String>,
        val groups: Map<Long, String>,
        val targetScreens: List<String>
)

data class NotificationReadStatusUpdateRequest(
        val notificationIdList: List<Long>
)

data class NotificationReadStatusUpdateResponse(
        val updatedNotificationIds: List<Long>
)

data class FirebaseTokenResult(
        val firebaseIdToken: String,
        val platform: String
)