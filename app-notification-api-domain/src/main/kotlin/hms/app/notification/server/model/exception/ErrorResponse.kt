package hms.app.notification.server.model.exception

data class ErrorResponse(var errorCode: String,
                         var errorDescription:String) {
}