# App Notification API Server

## Software Used

* Java 1.8
* Gradle 4.0.1
* Kotlin 1.1.3-2
* MySQL 5.7
* Postman 6.0.10

## DNS Configurations

Add following DNS entries to your `/etc/hosts` file.

```
127.0.0.1 db.mysql.appnotification
```


## Setup initial data

* Create a MySQL user according to the following details.
    - username: user
    - password: password
* Create a new MySQL database 'appnotification_db' (only if there is no database named 'appnotification_db').
* Run the MySQL script **appnotifiaction_db.sql** in the `<project-location>/db-setup`.
* Please keep a backup of databases before applying dumps.

## Generate and retrieve Firebase access token

* Every Firebase project has a default service account. You can use this account to call Firebase server APIs from your 
app server or trusted environment. If you use a different service account, make sure it has Editor or Owner permissions.
 
* To authenticate the service account and authorize it to access Firebase services, 
you must generate a private key file in JSON format and use this key to retrieve a short-lived OAuth 2.0 token. 
Once you have a valid token, you can add it in your server requests as required by the various Firebase services such as Remote Config or FCM.

* Please refer the following link to find complete instructions about access token,
https://firebase.google.com/docs/cloud-messaging/migrate-v1

## Set date time

* Date and the time of the server which is going to run the notification server should have correct date and time according to the time zone.
If there is an issue with the date time in the server, it will throw an exception as mentioned below.

`Invalid JWT: Token must be a short-lived token (60 minutes) and in a reasonable timeframe. 
Check your iat and exp values and use a clock with skew to account for clock differences between systems.`

## How to Build and create Distribution

* Run `./gradlew clean` to clean the project.

* Run `./gradlew -p app-notification-api-service createTanukiWrapper`

* Distribution file will be created at `<project-location>/app-notification-api-service/build/app-notification-api-service`

* Use email address dev.vodafone.appstore@gmail.com and password test123# for accessing the Firebase console

* To test the notification display in device use the sample app in ./sample-app directory

## How to start the server

* Build as mentioned in the previous step.

* Go to `<project-location>/app-notification-api-service/build/app-notification-api-service/bin`

* Execute `./appstore-notification-admin-api-service start` to start the server

## Configurations

Configurations can be found in the application.yml file.

#### Configure MySQL
* `spring:datasource:url`: MySQL connection URL 
* `spring:datasource:username`: Username of the MySQL user
* `spring:datasource:password`: Password of the MySQL user

#### Server related configurations
* `firebase.cloud.messaging.config:defaultFcmTopicName`: Topic for the all users
* `firebase.cloud.messaging.config:serverKey`: Firebase server key of the project
* `firebase.cloud.messaging.config:androidChannel`: Messaging Android channel
* `firebase.cloud.messaging.config:iidServerUrl`: Instance ID server URL
* `firebase.cloud.messaging.config:fcmServerUrl`: Firebase server URL
* `firebase.cloud.messaging.config:firebaseMessageSendUrl`: Firebase URL to send requests to dispatch push notifications. e.g: /v1/projects/[project-id]/messages:send. project-id should be replaced by Firebase project id.
* `firebase.cloud.messaging.config:accessTokenGenerateUrl`: URL using to validate the Firebase service account and generate access token to be sent Firebase request header
* `firebase.cloud.messaging.config:defaultDispatchType`: API support (new or legacy)
* `firebase.cloud.messaging.config:messageDispatchDateTimeFormat`: Format of the message dispatched date time (store in the MySQL database, message_dispatch_history table).
* `firebase.cloud.messaging.config:accessTokenFileName`: Name of the JSON file downloaded by Firebase service account.

#### Forward proxy configurations
* `forward.proxy.config:enabled`: Enable or disable the forward proxy configurations.
* `forward.proxy.config:host`: Proxy host
* `forward.proxy.config:port`: Proxy port