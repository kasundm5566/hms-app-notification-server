package hms.appnotification.integrations.fcm

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

/**
 * Created by kasun on 7/16/18.
 */
interface UpdatedFirebaseMessagingClient{

    fun notifyTopic(requestId: String, topic: String,
                           notification: UpdatedFirebaseNotificationBody, clickAction: String): Mono<FirebaseNotifyResp>

    fun notifyUsersAndroid(requestId: String, firebaseIdTokens: List<String>, notification: UpdatedFirebaseNotificationBody,
                           additionalParameters: Map<String, String>, clickAction: String, msisdn: String): Mono<FirebaseNotifyResp>

    fun retrieveMessagesSentForUser(userId: String, firebaseToken: String, start: Int, limit: Int): Mono<DispatchedMessagesForUserResponse>
}