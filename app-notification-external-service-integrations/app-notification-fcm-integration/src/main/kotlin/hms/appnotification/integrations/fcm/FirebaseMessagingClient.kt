package hms.appnotification.client

import hms.app.notification.server.model.notification.NotificationContent
import hms.appnotification.integrations.fcm.FirebaseTokenNotifyResp
import hms.appnotification.integrations.fcm.FirebaseTopicNotifyResp
import reactor.core.publisher.Mono

interface FirebaseMessagingClient {

    fun notifyUsersAndroid(firebaseIdTokens: List<String>,
                           notificationContent: NotificationContent,
                           serverKey: String): Mono<FirebaseTokenNotifyResp>

    fun notifyUsersIOS(firebaseIdTokens: List<String>,
                    notificationContent: NotificationContent,
                    serverKey: String): Mono<FirebaseTokenNotifyResp>

    fun notifyTopic(topic: String,
                    notificationContent: NotificationContent,
                    serverKey: String): Mono<FirebaseTopicNotifyResp>

    fun subscribeUserToTopic(topic: String, token: String, serverkey: String, userId: String): Mono<String>

}