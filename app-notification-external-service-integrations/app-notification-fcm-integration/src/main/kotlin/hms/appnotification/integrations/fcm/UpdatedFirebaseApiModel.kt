package hms.appnotification.integrations.fcm

import com.fasterxml.jackson.annotation.JsonProperty
import hms.appnotification.repo.entity.MessageDispatchHistoryEntity

/**
 * Created by kasun on 7/16/18.
 */
data class UpdatedFirebaseNotificationBody(
        @JsonProperty("title") val title: String,
        @JsonProperty("body") val body: String
)

data class FirebaseNotifyResp(@JsonProperty("name") val name: String)

data class UpdatedFirebaseDataBody(
        @JsonProperty("story_id") val story_id: String
)

data class FirebaseAndroidNotificationDataBody(
        @JsonProperty("click_action") val click_action: String
)

data class FirebaseAndroidDataBody(
        @JsonProperty("notification") val notification: FirebaseAndroidNotificationDataBody
)

@Deprecated("This formatting lead to create issues when receiving notifications when app is closed.")
data class FirebaseNotificationMessageBody(
        @JsonProperty("topic") val topic: String,
        @JsonProperty("notification") val notification: UpdatedFirebaseNotificationBody,
        @JsonProperty("android") val androidBoby: FirebaseAndroidDataBody
)

data class FirebaseNotificationContent(
        @JsonProperty("topic") val topic: String,
        @JsonProperty("data") val data: Map<String, String>
)

data class FirebaseNotificationContentTokenSpecific(
        @JsonProperty("token") val token: String,
        @JsonProperty("data") val data: Map<String, String>
)

@Deprecated("This formatting lead to create issues when receiving notifications when app is closed.")
data class FirebaseNotificationMessage(
        @JsonProperty("message") val message: FirebaseNotificationMessageBody
)

data class FirebaseNotification(
        @JsonProperty("message") val message: FirebaseNotificationContent
)

data class FirebaseNotificationByToken(
        @JsonProperty("message") val message: FirebaseNotificationContentTokenSpecific
)

@Deprecated("This formatting lead to create issues when receiving notifications when app is closed.")
data class FirebaseAndroidNotificationMessage(
        @JsonProperty("message") val message: FirebaseAndroidNotificationMessageBody
)

@Deprecated("This formatting lead to create issues when receiving notifications when app is closed.")
data class FirebaseAndroidNotificationMessageBody(
        @JsonProperty("notification") val notification: UpdatedFirebaseNotificationBody,
        @JsonProperty("token") val token: String,
        @JsonProperty("data") val data: Map<String, String>,
        @JsonProperty("android") val androidBoby: FirebaseAndroidDataBody
)

data class DispatchedMessagesForUserResponse(
        val status: String,
        val messages: List<MessageDispatchHistoryEntity>
)

data class MessageDispatchHistory(
        val requestId: String,
        val messageType: String,
        val messageContent: String,
        val dispatchedTime: String
)
