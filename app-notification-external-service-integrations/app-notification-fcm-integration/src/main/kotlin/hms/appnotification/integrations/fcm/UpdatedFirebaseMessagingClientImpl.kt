package hms.appnotification.integrations.fcm

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential
import hms.appnotification.repo.entity.MessageDispatchHistoryEntity
import hms.appnotification.repo.repository.DeviceRegistrationRepository
import hms.appnotification.repo.repository.MessageDispatchHistoryRepository
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.core.io.ClassPathResource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.publisher.toFlux
import reactor.core.publisher.toMono
import java.io.FileInputStream
import java.io.IOException
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import javax.annotation.PostConstruct
import java.text.SimpleDateFormat
import java.text.DateFormat
import java.util.*
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.transaction.annotation.Transactional
import javax.persistence.EntityTransaction


/**
 * Created by kasun on 7/16/18.
 */
@Service
@ConfigurationProperties(prefix = "firebase.cloud.messaging.config")
@EnableConfigurationProperties
class UpdatedFirebaseMessagingClientImpl(val deviceRegistrationRepository: DeviceRegistrationRepository,
                                         val messageDispatchHistoryRepository: MessageDispatchHistoryRepository) : UpdatedFirebaseMessagingClient {

    var androidChannel: String? = ""
    var iidServerUrl: String? = ""
    var fcmServerUrl: String? = ""
    var accessTokenFileName: String? = ""
    var messageDispatchDateTimeFormat: String? = ""
    var firebaseMessageSendUrl: String? = ""
    var accessTokenGenerateUrl: String? = ""
    private var googleIidClient: WebClient? = null
    private var googleFcmClient: WebClient? = null
    @Value("\${forward.proxy.config.enabled:}")
    var forwardProxyEnabled: Boolean = false
    @Value("\${forward.proxy.config.host:}")
    var host: String? = ""
    @Value("\${forward.proxy.config.port:}")
    var port: Int = 0

    companion object {
        val LOGGER = LoggerFactory.getLogger(UpdatedFirebaseMessagingClientImpl::class.java)
    }

    enum class DispatcherType {
        INDIVIDUAL, TOPIC
    }

    @PostConstruct
    fun initClient() {
        googleIidClient = createWebClient(iidServerUrl)
        googleFcmClient = createWebClient(fcmServerUrl)
    }

    private fun createWebClient(serverUrl: String?): WebClient? {
        return serverUrl.let { url ->
            if (forwardProxyEnabled) {
                val connector = ReactorClientHttpConnector { options ->
                    options.httpProxy { addressSpec ->
                        addressSpec.host(host).port(port)
                    }
                }
                UpdatedFirebaseMessagingClientImpl.LOGGER.debug("Creating web client with forward proxy settings for [$url]: host: [$host] port: [$port]")
                WebClient.builder()
                        .clientConnector(connector)
                        .baseUrl(url)
                        .build()
            } else {
                UpdatedFirebaseMessagingClientImpl.LOGGER.debug("Creating web client for [$url]")
                WebClient.builder()
                        .baseUrl(url)
                        .build()
            }
        }

    }

    override fun notifyTopic(requestId: String, topic: String, notification: UpdatedFirebaseNotificationBody, clickAction: String): Mono<FirebaseNotifyResp> {
        UpdatedFirebaseMessagingClientImpl.LOGGER.info("Notification Request with [$notification], topic [$topic] to [ANDROID]")
        val client = googleFcmClient
        if (client != null) {
            return client.post()
                    .uri(firebaseMessageSendUrl)
                    .header(HttpHeaders.AUTHORIZATION,
                            "Bearer " + getAccessToken())
                    .header(HttpHeaders.CONTENT_TYPE,
                            MediaType.APPLICATION_JSON_UTF8_VALUE)
                    .body(BodyInserters.fromObject(getMessageBodyForNotificationUpdated(requestId, topic, notification, clickAction)))
                    .retrieve()
                    .bodyToMono(FirebaseNotifyResp::class.java)
                    .doOnError { error -> UpdatedFirebaseMessagingClientImpl.LOGGER.error("Error Response received [ANDROID] [$error]") }
                    .doOnSuccess { success ->
                        val formatter = DateTimeFormatter.ofPattern(messageDispatchDateTimeFormat)
                        val mapper = ObjectMapper()
                        messageDispatchHistoryRepository.save(MessageDispatchHistoryEntity(1,
                                ZonedDateTime.now().format(formatter).toString(), requestId, topic, notification.body,
                                DispatcherType.TOPIC.toString(), "", clickAction,
                                mapper.writeValueAsString(hashMapOf<String, String>("requestId" to requestId, "click_action" to clickAction,
                                        "title" to notification.title, "body" to notification.body))))
                        UpdatedFirebaseMessagingClientImpl.LOGGER.info("Success response received [ANDROID] [$success]")
                    }
        } else {
            return Mono.empty()
        }
    }

    @Throws(IOException::class)
    private fun getAccessToken(): String {
        val MESSAGING_SCOPE = accessTokenGenerateUrl.toString();
        val SCOPES = arrayOf<String>(MESSAGING_SCOPE)
        val googleCredential = GoogleCredential
                .fromStream(FileInputStream(ClassPathResource(accessTokenFileName).getFile()))
                .createScoped(SCOPES.toMutableList())
        googleCredential.refreshToken()
        return googleCredential.accessToken
    }

    @Deprecated("This request formation lead to create issues when receiving notifications when app is closed.")
    private fun getMessageBodyForNotification(topic: String,
                                              notification: UpdatedFirebaseNotificationBody, clickAction: String): FirebaseNotificationMessage {
        return FirebaseNotificationMessage(
                FirebaseNotificationMessageBody(topic, notification, FirebaseAndroidDataBody(FirebaseAndroidNotificationDataBody(clickAction)))
        )
    }

    private fun getMessageBodyForNotificationUpdated(requestId: String, topic: String,
                                                     notification: UpdatedFirebaseNotificationBody, clickAction: String): FirebaseNotification {
        val dataMap = hashMapOf<String, String>("requestId" to requestId, "title" to notification.title, "body" to notification.body, "click_action" to clickAction, "appId" to "")
        return FirebaseNotification(
                FirebaseNotificationContent(topic, dataMap)
        )
    }

    override fun notifyUsersAndroid(requestId: String, firebaseIdTokens: List<String>,
                                    notification: UpdatedFirebaseNotificationBody, additionalParameters: Map<String, String>,
                                    clickAction: String, msisdn: String): Mono<FirebaseNotifyResp> {

        if (firebaseIdTokens.isEmpty()) {
            UpdatedFirebaseMessagingClientImpl.LOGGER.debug("Empty fcm token list received")
            return Mono.empty()
        }

        UpdatedFirebaseMessagingClientImpl.LOGGER.info("Notification Request with [$notification] to [ANDROID] [$firebaseIdTokens]")
//        val successResponses = ArrayList<String>()
//        val notificationMessage = getMessageBodyForAndroid(notification, firebaseIdTokens.get(0))
        val responses = notifyUsers(requestId, firebaseIdTokens, notification, additionalParameters, clickAction, msisdn)
        val resMsgs = ArrayList<String>()
        responses.subscribe()
/*        responses.subscribe {
            it.subscribe()
        }*/
        return FirebaseNotifyResp(resMsgs.toString()).toMono()
    }

    private fun notifyUsers(requestId: String, firebaseIdTokens: List<String>,
                            notification: UpdatedFirebaseNotificationBody, additionalParameters: Map<String, String>,
                            clickAction: String, msisdn: String): Flux<Mono<FirebaseNotifyResp>> {
        var map: Flux<Mono<FirebaseNotifyResp>> = Flux.fromIterable(firebaseIdTokens)
                .map { fbToken ->
                    sendIndividual(requestId, fbToken, notification, additionalParameters, clickAction, msisdn)
                }
        map.subscribe {
            it.subscribe()
        }
        return map.toFlux()
    }

    private fun sendIndividual(requestId: String, fbToken: String, notification: UpdatedFirebaseNotificationBody,
                               additionalParameters: Map<String, String>, clickAction: String, userId: String): Mono<FirebaseNotifyResp> {
        val client = googleFcmClient
        val notificationMessage = getMessageBodyForAndroidUpdated(requestId, notification, fbToken, additionalParameters, clickAction)
        if (client != null) {
            return client.post()
                    .uri(firebaseMessageSendUrl)
                    .header(HttpHeaders.AUTHORIZATION,
                            "Bearer " + getAccessToken())
                    .header(HttpHeaders.CONTENT_TYPE,
                            MediaType.APPLICATION_JSON_UTF8_VALUE)
                    .body(BodyInserters.fromObject(notificationMessage))
                    .retrieve()
                    .bodyToMono(FirebaseNotifyResp::class.java)
                    .doOnError { error -> UpdatedFirebaseMessagingClientImpl.LOGGER.error("Error Response received [ANDROID] [$error]") }
                    .doOnSuccess { success ->
                        val formatter = DateTimeFormatter.ofPattern(messageDispatchDateTimeFormat);
                        val mapper = ObjectMapper()
                        messageDispatchHistoryRepository.save(MessageDispatchHistoryEntity(1,
                                ZonedDateTime.now().format(formatter).toString(), requestId, userId,
                                notification.body, DispatcherType.INDIVIDUAL.toString(), fbToken, clickAction,
                                mapper.writeValueAsString(hashMapOf("requestId" to requestId, "appId" to additionalParameters.get("appId"),
                                        "click_action" to clickAction, "title" to notification.title, "body" to notification.body))))
                        UpdatedFirebaseMessagingClientImpl.LOGGER.info("Success response received [ANDROID] [$success]")
                    }
        } else {
            return Mono.empty()
        }
    }

    private fun getUserId(fcmToken: String): String {
        return deviceRegistrationRepository.findByToken(fcmToken)
    }

    @Deprecated("This request formation lead to create issues when receiving notifications when app is closed.")
    private fun getMessageBodyForAndroid(notificationContent: UpdatedFirebaseNotificationBody, token: String,
                                         additionalParameters: Map<String, String>, clickAction: String): FirebaseAndroidNotificationMessage {
        return FirebaseAndroidNotificationMessage(
                FirebaseAndroidNotificationMessageBody(notificationContent, token,
                        hashMapOf("appId" to additionalParameters.get("appId").toString()),
                        FirebaseAndroidDataBody(FirebaseAndroidNotificationDataBody(clickAction)))
        )
    }

    private fun getMessageBodyForAndroidUpdated(requestId: String, notificationContent: UpdatedFirebaseNotificationBody, token: String,
                                                additionalParameters: Map<String, String>, clickAction: String): FirebaseNotificationByToken {
        val dataMap = hashMapOf<String, String>("requestId" to requestId, "title" to notificationContent.title,
                "body" to notificationContent.body, "click_action" to clickAction, "appId" to additionalParameters.get("appId").toString())
        return FirebaseNotificationByToken(
                FirebaseNotificationContentTokenSpecific(token, dataMap)
        )
    }

    override fun retrieveMessagesSentForUser(userId: String, firebaseToken: String, start: Int, limit: Int): Mono<DispatchedMessagesForUserResponse> {
        val messages = messageDispatchHistoryRepository.findMessagesByUserId(userId, firebaseToken, start, limit)
        return DispatchedMessagesForUserResponse("S1000", messages).toMono()
    }
}