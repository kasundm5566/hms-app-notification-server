package hms.appnotification.integrations.fcm

import com.fasterxml.jackson.annotation.JsonProperty

data class FirebaseTokenNotifyResp(@JsonProperty("multicast_id") val multicastId: Long,
                                   @JsonProperty("success") val success: Int,
                                   @JsonProperty("failure") val failure: Int,
                                   @JsonProperty("canonical_ids") val canonicalIds: Int,
                                   @JsonProperty("results") val results: List<Map<String, String>>)

data class FirebaseTopicNotifyResp(
        @JsonProperty("message_id") val messageId: Long,
        @JsonProperty("error") val error: Int
)

data class FirebaseMessageReqBodyAndroid(
        @JsonProperty("registration_ids") val registrationIDs: List<String>,
        @JsonProperty("data") val data: FirebaseDataBody
)

data class FirebaseTopicMessageReqBodyAndroid(
        @JsonProperty("to") val registrationIDs: String,
        @JsonProperty("data") val data: FirebaseDataBody
)

data class FirebaseMessageReqBodyIOS(
        @JsonProperty("registration_ids") val registrationIDs: List<String>,
        @JsonProperty("notification") val notification: FirebaseNotificationBody,
        @JsonProperty("data") val data: FirebaseDataBody

)

data class FirebaseNotificationBody(
        @JsonProperty("title") val title: String,
        @JsonProperty("body") val body: String
)

data class FirebaseDataBody(
        @JsonProperty("title") val title: String,
        @JsonProperty("body") val body: String,
        @JsonProperty("targetScreen") val targetScreen: String,
        @JsonProperty("android_channel_id") val notificationChannel: String?
)

data class FirebaseSubscribeToTopic(
        @JsonProperty("to") val to: String,
        @JsonProperty("registration_tokens") val tokens: Array<String>
)