package hms.appnotification.client

import hms.app.notification.server.model.notification.NotificationContent
import hms.appnotification.integrations.fcm.*
import hms.appnotification.repo.entity.UserTopicsEntity
import hms.appnotification.repo.entity.UserTopicsEntityId
import hms.appnotification.repo.repository.MessageDispatchHistoryRepository
import hms.appnotification.repo.repository.UserTopicsRepository
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import javax.annotation.PostConstruct

@Service
@ConfigurationProperties(prefix = "firebase.cloud.messaging.config")
@EnableConfigurationProperties
class FirebaseMessagingClientImpl(val userTopicsRepository: UserTopicsRepository) : FirebaseMessagingClient {

    var androidChannel: String? = ""
    var iidServerUrl: String? = ""
    var fcmServerUrl: String? = ""
    private var googleIidClient: WebClient? = null
    private var googleFcmClient: WebClient? = null

    companion object {
        val LOGGER = LoggerFactory.getLogger(FirebaseMessagingClientImpl::class.java)
    }

    @PostConstruct
    fun initClient() {
        googleIidClient = createWebClient(iidServerUrl)
        googleFcmClient = createWebClient(fcmServerUrl)
    }

    private fun createWebClient(serverUrl: String?): WebClient? {
        return serverUrl.let { url ->
            LOGGER.debug("Creating web client for [$url]")
            WebClient.builder()
                    .baseUrl(url)
                    .build()
        }

    }

    override fun notifyUsersAndroid(firebaseIdTokens: List<String>,
                                    notificationContent: NotificationContent,
                                    serverKey: String): Mono<FirebaseTokenNotifyResp> {

        if (firebaseIdTokens.isEmpty()) {
            LOGGER.debug("Empty fcm token list received")
            return Mono.empty()
        }

        LOGGER.info("Notification Request with [$notificationContent] to [ANDROID] [$firebaseIdTokens]")
        val client = googleFcmClient
        if (client != null) {
            return client.post()
                    .uri("/fcm/send")
                    .header(HttpHeaders.AUTHORIZATION,
                            "key=$serverKey")
                    .header(HttpHeaders.CONTENT_TYPE,
                            MediaType.APPLICATION_JSON_VALUE)
                    .body(BodyInserters.fromObject(getTokenMessageBodyForAndroid(firebaseIdTokens, notificationContent)))
                    .retrieve()
                    .bodyToMono(FirebaseTokenNotifyResp::class.java)
                    .doOnError { error -> LOGGER.error("Error Response received [ANDROID] [$error]") }
                    .doOnSuccess { success -> LOGGER.info("Success response received [ANDROID] [$success]") }
        } else {
            return Mono.empty()
        }

    }

    override fun notifyUsersIOS(firebaseIdTokens: List<String>, notificationContent: NotificationContent, serverKey: String): Mono<FirebaseTokenNotifyResp> {

        if (firebaseIdTokens.isEmpty()) {
            LOGGER.debug("Empty fcm token list received")
            return Mono.empty()
        }

        LOGGER.info("Notification Request with [$notificationContent] to [IOS] [$firebaseIdTokens]")
        val client = googleFcmClient
        if (client != null) {
            return client.post()
                    .uri("/fcm/send")
                    .header(HttpHeaders.AUTHORIZATION,
                            "key=$serverKey")
                    .header(HttpHeaders.CONTENT_TYPE,
                            MediaType.APPLICATION_JSON_VALUE)
                    .body(BodyInserters.fromObject(getTokenMessageBodyForIOS(firebaseIdTokens, notificationContent)))
                    .retrieve()
                    .bodyToMono(FirebaseTokenNotifyResp::class.java)
                    .doOnError { error -> LOGGER.error("Error Response received [IOS] [$error]") }
                    .doOnSuccess { success -> LOGGER.info("Success response received [IOS] [$success]") }
        } else {
            return Mono.empty()
        }

    }

    private fun getTokenMessageBodyForAndroid(firebaseIdTokens: List<String>,
                                              notificationContent: NotificationContent): FirebaseMessageReqBodyAndroid {
        return FirebaseMessageReqBodyAndroid(
                firebaseIdTokens,
                FirebaseDataBody(
                        notificationContent.title,
                        notificationContent.body,
                        notificationContent.targetScreen,
                        androidChannel
                )
        )
    }

    private fun getTopicMessageBodyForAndroid(firebaseTopic: String,
                                              notificationContent: NotificationContent): FirebaseTopicMessageReqBodyAndroid {
        return FirebaseTopicMessageReqBodyAndroid(
                "/topics/$firebaseTopic",
                FirebaseDataBody(
                        notificationContent.title,
                        notificationContent.body,
                        notificationContent.targetScreen,
                        androidChannel
                )
        )
    }

    private fun getTokenMessageBodyForIOS(firebaseIdTokens: List<String>,
                                          notificationContent: NotificationContent): FirebaseMessageReqBodyIOS {
        return FirebaseMessageReqBodyIOS(
                firebaseIdTokens,
                FirebaseNotificationBody(
                        notificationContent.title,
                        notificationContent.body
                ),
                FirebaseDataBody(
                        notificationContent.title,
                        notificationContent.body,
                        notificationContent.targetScreen,
                        "dev.vodafone.default.topic"
                )
        )
    }

    override fun notifyTopic(topic: String,
                             notificationContent: NotificationContent,
                             serverKey: String): Mono<FirebaseTopicNotifyResp> {
        LOGGER.info("Notification Request with [$notificationContent] to [ANDROID] topic [$topic]")
        val client = googleFcmClient
        if (client != null) {
            return client.post()
                    .uri("/fcm/send")
                    .header(HttpHeaders.AUTHORIZATION,
                            "key=$serverKey")
                    .header(HttpHeaders.CONTENT_TYPE,
                            MediaType.APPLICATION_JSON_VALUE)
                    .body(BodyInserters.fromObject(getTopicMessageBodyForAndroid(topic, notificationContent)))
                    .retrieve()
                    .bodyToMono(FirebaseTopicNotifyResp::class.java)
                    .doOnError { error -> LOGGER.error("Error Response received [ANDROID] [$error]") }
                    .doOnSuccess { success -> LOGGER.info("Success response received [ANDROID] [$success]") }
        } else {
            return Mono.empty()
        }
    }

    override fun subscribeUserToTopic(topic: String, token: String, serverkey: String, userId: String): Mono<String> {
        if (isEmptyString(topic) || isEmptyString(token) || isEmptyString(serverkey)) {
            LOGGER.error("Invalid request, topic, token or serverkey can't be empty")
            return Mono.empty()
        } else {
            val webClient = googleIidClient
            if (webClient != null) {
                return webClient
                        .post()
                        .uri("/iid/v1:batchAdd")
                        .header(HttpHeaders.AUTHORIZATION,
                                "key=$serverkey")
                        .header(HttpHeaders.CONTENT_TYPE,
                                MediaType.APPLICATION_JSON_VALUE)
                        .body(BodyInserters.fromObject(FirebaseSubscribeToTopic("/topics/$topic", arrayOf(token))))
                        .retrieve()
                        .bodyToMono(String::class.java)
                        .doOnError { error -> LOGGER.error("Error subscribing to topic [$topic] by [$token], Error [$error]") }
                        .doOnSuccess { success ->
                            userTopicsRepository.save(UserTopicsEntity(UserTopicsEntityId(userId, topic)))
                            LOGGER.info("Successfully subscribed to topic [$topic] by [$token], Response[$success]")
                        }
            } else {
                return Mono.empty()
            }
        }
    }

    private fun isEmptyString(str: String): Boolean {
        return !(str != null && str.trim().length > 0)
    }
}