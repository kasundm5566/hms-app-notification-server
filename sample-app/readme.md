## Sample App readme

* This app uses google fcm dependencies, this requires a google account to access firebase console
* Default account used is dev.vodafone.appstore@gmail.com : password test123#
* App target API level is 23

## Build Instructions

* Configure the build configurations(app-notification-server-url, notification-channel-id etc) in build.gradle file
* Execute './gradlew assembleDebug' command to build the apk
* You can find the apk file in the ./build/outputs/apk/ directory
* Alternatively if the device/emulator is already connected in debug mode, run './gradlew assembleDebug' command to build and install to the device in one go

