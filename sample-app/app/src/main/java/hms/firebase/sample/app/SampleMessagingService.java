package hms.firebase.sample.app;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


/*
*
*
    This class is a sample implementation of the FirebaseMessagingService which is to be used when the messages are required to be processed
    by the application. Currently not used in this sample app, notifications are allowed to be handled by the android OS
 */
public class SampleMessagingService extends FirebaseMessagingService{
    private static final String TAG = "SampleMessagingService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }
}
