package hms.firebase.sample.app;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.android.volley.Request;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

public class SampleFirebaseInstanceIDService extends FirebaseInstanceIdService{
    private static final String TAG = "SampleInstanceIDService";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        registerDevice(refreshedToken);
        subscribeToTopic();
    }

    private void subscribeToTopic() {
        FirebaseMessaging.getInstance().subscribeToTopic(BuildConfig.firebase_topic_name)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = "Subscribed to the topic successfully";
                        if (!task.isSuccessful()) {
                            msg = "Subscription to the topic failed";
                        }
                        Log.d(TAG, msg);
                        Toast.makeText(SampleFirebaseInstanceIDService.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void registerDevice(String token) {
        DeviceRegistrationRequest request = new DeviceRegistrationRequest();
        request.deviceId = "0701234567";
        request.userId = "0701234567";
        request.platform = "Android";
        request.platformVersion = "27";
        request.notificationToken = token;
        Gson gson = new Gson();
        String requestContent = gson.toJson(request);

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = BuildConfig.notification_server_url;
        JsonRequest<DeviceRegistrationResponse> registrationRequest = new JsonRequest<DeviceRegistrationResponse>(Request.Method.POST,
                url, requestContent, new Response.Listener<DeviceRegistrationResponse>() {
            @Override
            public void onResponse(DeviceRegistrationResponse response) {
                Log.d(TAG, "Device registration was successful");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Device registration failed with error " + error);
            }
        }) {
            @Override
            protected Response<DeviceRegistrationResponse> parseNetworkResponse(NetworkResponse response) {
                if (response.statusCode == 200) {
                    Log.d(TAG, "Device registration was successful");
                } else {
                    Log.d(TAG, "Device registration request failed with status : " + response.statusCode);
                }
                return Response.success(new DeviceRegistrationResponse(), null);
            }
        };
        queue.add(registrationRequest);
    }

    public static class DeviceRegistrationResponse{

    }

    public static class DeviceRegistrationRequest{
        public String deviceId;
        public String userId;
        public String platform;
        public String platformVersion;
        public String notificationToken;
    }
}